//
//  main.cpp
//  Mnist_creator
//
//  Created by ALEXANDRE  R MELLO on 23/09/16.
//  Copyright © 2016 ALEXANDRE  R MELLO. All rights reserved.
//


//Utilizar opencv e boost
//Chamar a função exec_train ou exec_test para criar os datasets.
//exec_train(diretorio_principal com subpastas, diretorio de saida dos datasets,int width,int height,bool shuffle images);
//exec_test(diretorio_principal sem subpastas, diretorio de saida dos datasets,int width,int height,bool shuffle images);

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <random>
#include <stdlib.h>

#include <opencv/cv.hpp>
#include <opencv2/highgui.hpp>

#include "boost/filesystem.hpp"
#include <boost/algorithm/string.hpp>
#include<boost/regex.hpp>

#include <boost/foreach.hpp>
#include <boost/detail/endian.hpp>

#include "util.h"
#include "cmdline.h"


using namespace std;
namespace fs = boost::filesystem;

struct image {
    cv::Mat data;
    uint8_t label;
};

template <typename ImageIterator>
void gen_mnist_labels(const string& label_file, ImageIterator begin, ImageIterator end)
{
    ofstream ofs(label_file.c_str(), ios::binary|ios::out);
    
    if (ofs.bad() || ofs.fail())
        throw runtime_error("failed to open file:" + label_file);
    
    uint32_t magic_number = 0x00000801;
    uint32_t num_items = distance(begin, end);
    
#if defined(BOOST_LITTLE_ENDIAN)
    reverse_endian(&magic_number);
    reverse_endian(&num_items);
#endif
    
    ofs.write((char*) &magic_number, 4);
    ofs.write((char*) &num_items, 4);
    
    for (; begin != end; ++begin)
        ofs.write((char*) &begin->label, 1);
}

template <typename ImageIterator>
void gen_mnist_images(const string& image_file, ImageIterator begin, ImageIterator end)
{
    ofstream ofs(image_file.c_str(), ios::binary | ios::out);
    
    if (ofs.bad() || ofs.fail())
        throw runtime_error("failed to open file:" + image_file);
    
    uint32_t magic_number = 0x00000803;
    uint32_t num_items = distance(begin, end);
    uint32_t num_rows = begin->data.rows;
    uint32_t num_cols = begin->data.cols;
    
#if defined(BOOST_LITTLE_ENDIAN)
    reverse_endian(&magic_number);
    reverse_endian(&num_items);
    reverse_endian(&num_rows);
    reverse_endian(&num_cols);
#endif
    
    ofs.write((char*) &magic_number, 4);
    ofs.write((char*) &num_items, 4);
    ofs.write((char*) &num_rows, 4);
    ofs.write((char*) &num_cols, 4);
    
    for (; begin != end; ++begin) {
        cv::Mat_<uint8_t>& m = (cv::Mat_<uint8_t>&)begin->data;
        
        for (auto v : m)
            ofs.write((const char*)&v, 1);
    }
}

std::vector<uint8_t> convert(const std::string& s)
{
    if (s.size() % 2 != 0) {
        throw std::runtime_error("Bad size argument");
    }
    std::vector<uint8_t> res;
    res.reserve(s.size() / 2);
    for (std::size_t i = 0, size = s.size(); i != size; i += 2) {
        std::size_t pos = 0;
        res.push_back(std::stoi(s.substr(i, 2), &pos, 16));
        if (pos != 2) {
            throw std::runtime_error("bad character in argument");
        }
    }
    return res;
}


int read_images(const fs::path& path, uint8_t label, vector<image>& images, int w, int h)
{
    int num_images = 0;
    BOOST_FOREACH(const fs::path p, std::make_pair(fs::directory_iterator(path), fs::directory_iterator())) {
        if (fs::is_directory(p)) continue;
        
        image img;
        cv::Mat srcimg = cv::imread(p.string(), cv::IMREAD_GRAYSCALE);
        if (srcimg.data == nullptr)
            continue;
        
        cv::resize(srcimg, img.data, cv::Size(w, h)); // gray, linear interpolation
        
        //cv::imshow("data", img.data);
        //cv::waitKey(5);
        
        img.label = label;
        images.push_back(img);
        num_images++;
    }
    return num_images;
}

void dump_map(const map<string, uint8_t>& m, const string& filename)
{
    ofstream ofs(filename.c_str());
    if (ofs.fail() || ofs.bad())
        throw runtime_error("failed to create file:" + filename);
    
    for (auto v : m)
        ofs << v.first << "," << v.second << endl;
}

string add_prefix(const string& prefix, const string& base)
{
    return prefix.empty() ? base : prefix + "_" + base;
}

void exec_train(const string& dir, const string& output_prefix, int w, int h, bool data_shuffle = true)
{
    fs::path path(dir);
    auto map = make_table_from_subdir_to_label(dir);
    vector<image> images;

    int numberoffiles=0;
    int label_pasta=0;
    
    
    for (boost::filesystem::directory_iterator itr(dir); itr!=boost::filesystem::directory_iterator(); ++itr)
    {
        cout << itr->path().filename() << ' '; // display filename only
        string filenm = itr->path().filename().string();
        
        if(filenm==".DS_Store")continue;
        label_pasta++;
        for (boost::filesystem::directory_iterator itr(dir+filenm); itr!=boost::filesystem::directory_iterator(); ++itr)
        {
            if(itr->path().filename().string()==".DS_Store")continue;
            cout << itr->path().filename() << ' '; // display filename only
            //string filenm = itr->path().filename().string();
            //cout<<filenm<<endl;
            
            numberoffiles++;
            
            if (is_regular_file(itr->status())) {
                
                image img;
                string dirimg = dir+filenm+"/"+itr->path().filename().string();
                
                
                cv::Mat srcimg = cv::imread(dirimg, cv::IMREAD_GRAYSCALE);
                
                if (srcimg.data == nullptr)
                    continue;
                
                cv::resize(srcimg, img.data, cv::Size(w, h)); // gray, linear interpolation
                
                const uint8_t *p = reinterpret_cast<const uint8_t*>((filenm).c_str());
                dirimg.assign(p, p + filenm.size());
                
                
                img.label = label_pasta; //type of images (int index number)
               // cout <<label_pasta<<endl;
                images.push_back(img);
            }
        }
    }

    // shuffle
    if (data_shuffle)
        shuffle(images.begin(), images.end(), default_random_engine(0));

    
    cout << "total " << images.size() << "images found." << endl;
    
    string train_img   = add_prefix(output_prefix, "train_images.idx3");
    string train_label = add_prefix(output_prefix, "train_labels.idx1");

    gen_mnist_images(train_img,   images.begin() , images.end());
    gen_mnist_labels(train_label, images.begin() , images.end());
    
    dump_map(map, "label.txt");
}
void exec_test(const string& dir, const string& output_prefix, int w, int h, bool data_shuffle = true)
{
    fs::path path(dir);
    auto map = make_table_from_subdir_to_label(dir);
    vector<image> images;
    
    int numberoffiles=0;
    int label_pasta=0;
    
    
    for (boost::filesystem::directory_iterator itr(dir); itr!=boost::filesystem::directory_iterator(); ++itr)
    {
        cout << itr->path().filename() << ' '; // display filename only
        string filenm = itr->path().filename().string();
        
        if(filenm==".DS_Store")continue;
  
  
            if(itr->path().filename().string()==".DS_Store")continue;
            cout << itr->path().filename() << ' '; // display filename only

            
            numberoffiles++;
            
            if (is_regular_file(itr->status())) {
                
                image img;
                string dirimg = dir+filenm;
                
                
                cv::Mat srcimg = cv::imread(dirimg, cv::IMREAD_GRAYSCALE);
                
                if (srcimg.data == nullptr)
                    continue;
                
                cv::resize(srcimg, img.data, cv::Size(w, h)); // gray, linear interpolation
                
                const uint8_t *p = reinterpret_cast<const uint8_t*>((filenm).c_str());
                dirimg.assign(p, p + filenm.size());
                
                
                img.label = label_pasta; //type of images (int index number)
                // cout <<label_pasta<<endl;
                images.push_back(img);
            }
        
    }
    
    // shuffle
    if (data_shuffle)
        shuffle(images.begin(), images.end(), default_random_engine(0));
    
    cout << "total " << images.size() << "images found." << endl;
    
    string test_img    = add_prefix(output_prefix, "test_images.idx3");
    string test_label  = add_prefix(output_prefix, "test_labels.idx1");
    

    gen_mnist_images(test_img,   images.begin() , images.end());
    gen_mnist_labels(test_label, images.begin() , images.end());
    
    dump_map(map, "label.txt");
}

int main(int argc, char *argv[])
{
    //string dir ="/Users/alexandrermello/Documents/C++/Mnist_creator/Mnist_creator/Train/";
    //string target= "/Users/alexandrermello/Documents/C++/Mnist_creator/Mnist_creator/Train/";
    //string output= "/Users/alexandrermello/Documents/C++/Mnist_creator/Mnist_creator/Train/au";
    
    string dir ="/Users/alexandrermello/Documents/C++/Mnist_creator/Mnist_creator/Teste/";
    string target= "/Users/alexandrermello/Documents/C++/Mnist_creator/Mnist_creator/Teste/";
    string output= "/Users/alexandrermello/Documents/C++/Mnist_creator/Mnist_creator/Teste/au";

    //int w, int h
    int wid=70;  //width
    int hei=70;  //height
    bool shuffle = false;
   //exec_train(dir, output,wid,hei, shuffle);
    exec_test(dir, output, wid,hei, true);
    

    
    
    return 0;
}